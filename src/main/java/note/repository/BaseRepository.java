package note.repository;

import java.io.File;
import java.io.IOException;

public class BaseRepository {
    protected void checkAndCreateFile(String filename){
        File file = new File(filename);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
