package note.repository;

import java.util.List;

import note.model.Nota;
import note.utils.ClasaException;
import note.utils.NoteException;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException, NoteException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
