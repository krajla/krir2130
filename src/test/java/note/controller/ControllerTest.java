package note.controller;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.repository.*;
import note.utils.ClasaException;
import note.utils.NoteException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ControllerTest {
    private ClasaRepository _clasaRepository;
    private NoteRepository _noteRepository;
    private Elev _elev1;
    @Test
    public void unitA() throws NoteException {
        Nota n1 = new Nota(3,"romana",10);
        try {
            _noteRepository.addNota(n1);
        } catch (ClasaException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void unitB(){
        try {
            List<Medie> medii = _clasaRepository.calculeazaMedii();
            for (Medie medie: medii) {
                if(medie.getElev() == _elev1)
                    assertTrue(medie.getMedie() == 7);
            }
        } catch (ClasaException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void unitC(){
        List<Corigent> corigenti = _clasaRepository.getCorigenti();
        assertTrue(corigenti.size() == 1);
        assertSame("b", corigenti.get(0).getNumeElev());
    }
    @Test
    public void integrationTestBigBang(){
        Controller ctrl = new Controller(new NoteRepositoryMock(), new ClasaRepositoryMock(), new EleviRepositoryMock());
        try {
            Elev e = new Elev(1, "boss");
            ctrl.addElev(e);
            ctrl.addNota(new Nota(1, "matematica", 2));
            ctrl.addNota(new Nota(1, "matematica", 4));

            //P->A->B->C
            ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
            List<Corigent> corigenti = ctrl.getCorigenti();
            assertTrue(corigenti.size() == 1);
            assertSame("boss", corigenti.get(0).getNumeElev());
        } catch (ClasaException e) {
            e.printStackTrace();
        } catch (NoteException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void integrationTopDown(){
        Controller ctrl = new Controller(new NoteRepositoryMock(), new ClasaRepositoryMock(), new EleviRepositoryMock());
        try {
            Elev e = new Elev(1, "boss");
            ctrl.addElev(e);
            ctrl.addNota(new Nota(1, "matematica", 2));
            ctrl.addNota(new Nota(1, "matematica", 4));
            //P->A
            assertTrue(ctrl.getNote().size() == 2);

            //P->A->B
            List<Medie> medii = ctrl.calculeazaMedii();
            for (Medie medie: medii) {
                if(medie.getElev() == e)
                    assertTrue(medie.getMedie() == 3);
            }

            //P->A->B->C
            ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
            List<Corigent> corigenti = ctrl.getCorigenti();
            assertTrue(corigenti.size() == 1);
            assertSame("boss", corigenti.get(0).getNumeElev());

        } catch (ClasaException e) {
            e.printStackTrace();
        } catch (NoteException e) {
            e.printStackTrace();
        }
    }
    @Before
    public void setUp() throws Exception {
        _clasaRepository = new ClasaRepositoryMock();
        _noteRepository = new NoteRepositoryMock();
        _elev1 = new Elev(1, "a");
        Elev elev2 = new Elev(2, "b");
        Nota nota1 = new Nota(1, "romana", 10);
        Nota nota2 = new Nota(1, "romana", 4);
        Nota nota3 = new Nota(2, "romana", 4);
        Nota nota4 = new Nota(2, "romana", 2);
        List<Elev> elevList = new ArrayList<>();
        List<Nota> notaList = new ArrayList<>();
        elevList.add(_elev1);
        elevList.add(elev2);
        notaList.add(nota1);
        notaList.add(nota2);
        notaList.add(nota3);
        notaList.add(nota4);

        _clasaRepository.creazaClasa(elevList, notaList);
    }

}