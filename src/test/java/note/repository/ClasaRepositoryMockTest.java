package note.repository;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;
import note.utils.ClasaException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ClasaRepositoryMockTest {
    private ClasaRepository _clasaRepository;
    private ClasaRepository _clasaRepositoryGol;
    private Elev _elev1;

    @Test
    public void calculeazaMediiPass() {
        try {
            List<Medie> medii = _clasaRepository.calculeazaMedii();
            for (Medie medie: medii) {
                if(medie.getElev() == _elev1)
                    assertTrue(medie.getMedie() == 7);
            }
        } catch (ClasaException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void calculeazaMediiFail() throws ClasaException {
        List<Medie> medies = _clasaRepositoryGol.calculeazaMedii();
        assertTrue(medies.size() == 0);
    }

    @Before
    public void setUp() throws Exception {
        _clasaRepository = new ClasaRepositoryMock();
        _clasaRepositoryGol = new ClasaRepositoryMock();
        _elev1 = new Elev(1, "a");
        Elev elev2 = new Elev(2, "b");
        Nota nota1 = new Nota(1, "romana", 10);
        Nota nota2 = new Nota(1, "romana", 4);
        Nota nota3 = new Nota(2, "romana", 4);
        Nota nota4 = new Nota(2, "romana", 2);
        List<Elev> elevList = new ArrayList<>();
        List<Nota> notaList = new ArrayList<>();
        elevList.add(_elev1);
        elevList.add(elev2);
        notaList.add(nota1);
        notaList.add(nota2);
        notaList.add(nota3);
        notaList.add(nota4);

        _clasaRepository.creazaClasa(elevList, notaList);
        _clasaRepositoryGol.creazaClasa(new ArrayList<Elev>(), new ArrayList<Nota>());
    }


    @Test
    public void getCorigentiPass() {
        List<Corigent> corigenti = _clasaRepository.getCorigenti();
        assertTrue(corigenti.size() == 1);
        assertSame("b", corigenti.get(0).getNumeElev());
    }

    @Test
    public void getCorigentiFail() {

        List<Corigent> corigents = _clasaRepositoryGol.getCorigenti();
        assertTrue(corigents.size() == 0);
    }
}