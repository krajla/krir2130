package note.repository;

import note.model.Nota;
import note.utils.ClasaException;
import note.utils.NoteException;
import org.junit.Before;
import org.junit.Test;

public class NoteRepositoryMockTest {
    private NoteRepository repo;

    @Before
    public void init(){
        repo = new NoteRepositoryMock();

    }
    @Test
    public void testValid() throws Exception {
        Nota n1 = new Nota(1,"romana",1);
        repo.addNota(n1);

    }
    @Test(expected = NoteException.class)
    public void testNotaInvalida() throws Exception {
        Nota n1 = new Nota(1,"romana",11);
        repo.addNota(n1);
    }
    @Test(expected = ClasaException.class)
    public void testInvalidNrMatricol () throws Exception {
          Nota n1 = new Nota(-1, "romana", 1);
          repo.addNota(n1);
    }
    @Test
    public void testLimitaMatricol () throws Exception {
        Nota n1 = new Nota(1000, "romana", 1);
        repo.addNota(n1);
    }
    @Test
    public void testLimitaNota() throws Exception{
        Nota n1 = new Nota(1,"romana", 10);
        repo.addNota(n1);
    }


}